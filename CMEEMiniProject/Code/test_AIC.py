"""runs AIC and BIC to test fit of models against total data"""
__author__ = "Laura Merritt lm1516@ic.ac.uk"
__version__ = "0.0.1"

import numpy as np
import csv
import math

# read in models table
f = open("../Data/ModelsminO.csv", "rb")
csvread = csv.reader(f)
modelminO = []
for row in csvread:
	modelminO.append(tuple(row))

f.close()

# read in total data
f = open("../Data/DataTotalminO.csv", "rb")
csvread = csv.reader(f)
DataTotalminO = []
for row in csvread:
	DataTotalminO.append(tuple(row))

f.close()

# model comparison
nminO = (len(DataTotalminO)-1)
kminO = 2
index = 0
index2 = 0
index3 = 0
index4 = len(DataTotalminO)-1
resminO = [0]*(len(DataTotalminO)-1)*(len(modelminO)-1)
rssminO = [0]*(len(modelminO)-1)
AICminO = [0]*(len(modelminO)-1)
BICminO = [0]*(len(modelminO)-1)

for i in modelminO[1:9]:
	for j in DataTotalminO[1:len(DataTotalminO)]:
		pred = float(i[0]) + float(i[1])*float(j[1])
		resminO[index] = (float(j[2]) - pred)**2
		index = index + 1
	rssminO[index2] = sum(resminO[index3:index4])
	AICminO[index2] = nminO*np.log((2*math.pi)/nminO) + nminO + 2 + nminO*np.log(rssminO[index2]) + 2*kminO
	BICminO[index2] = nminO + nminO*np.log(2*math.pi) + nminO*np.log(rssminO[index2]/nminO) + np.log(nminO)*(kminO + 1)
	index2 = index2 + 1
	index3 = index4
	index4 = index4 + len(DataTotalminO) - 1

# make sure all models/model comparison criterion are aligned correctly for zipping together
AICminO.insert(0, "AIC")
BICminO.insert(0, "BIC")
interceptsminO = [i[0] for i in modelminO]
slopesminO = [i[1] for i in modelminO]

resultsminO = zip(interceptsminO, slopesminO, AICminO, BICminO)

# write out results to csv file
f = open("../Results/test_AICResultsminO.csv", "wb")
csvwrite = csv.writer(f)
for row in resultsminO:
	print row
	csvwrite.writerow(row)

f.close()
