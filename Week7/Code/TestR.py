'''Example of using subprocess to run an R script from python'''
__author__ = 'Laura Merritt lm1516@ic.ac.uk'
__version__ = '0.0.1'

import subprocess
subprocess.Popen("/usr/lib/R/bin/Rscript --verbose TestR.R > \
 ../Results/TestR.Rout 2> ../Results/TestR_errFile.Rout",\
 shell = True).wait()

