'''example exercises showing the use of list comprehensions and conventional loops'''
__author__ = 'Laura Merritt lm1516@ic.ac.uk'
__version__ = "0.0.1"

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

latin_names_cl = [species[0] for species in birds]
print latin_names_cl

common_names_cl = [species[1] for species in birds]
print common_names_cl

mean_body_masses_cl = [species[2] for species in birds]
print mean_body_masses_cl

# (2) Now do the same using conventional loops (you can shoose to do this 
# before 1 !). 

latin_names_loops = []
for species in birds:
	latin_names_loops.append(species[0])
print latin_names_loops

common_names_loops = []
for species in birds:
	common_names_loops.append(species[1])
print common_names_loops

mean_body_masses_loops = []
for species in birds:
	mean_body_masses_loops.append(species[2])
print mean_body_masses_loops

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
