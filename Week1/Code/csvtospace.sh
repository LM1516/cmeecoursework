#!/bin/bash
# Author: Laura Merritt
# Script: csvtospace.sh
# Desc: substitute the commas in files to spaces
#       saves the output into a .txt file
# Arguements: 1-> comma separated variable file without .csv extension
# Date: Oct 2016

echo "Converting commas to spaces for $1.csv"

cat $1.csv | tr -s "," " " >> $1.txt

echo "done"

exit
