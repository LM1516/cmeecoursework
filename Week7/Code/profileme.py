'''example functions to test profiling on'''
__author__ = 'Laura Merritt lm1516@ic.ac.uk'
__version__ = '0.0.1'

def a_useless_function(x):
	'''adds numbers from 0 to 1E08'''
	y = 0
	# eight zeros!
	for i in xrange(100000000):
		y = y + i
	return 0

def a_less_useless_function(x):
	'''adds numbers from 0 to 1E05'''
	y = 0
	# five zeros!
	for i in xrange(100000):
		y = y + i
	return 0

def some_function(x):
	'''allows simultaneous running of a_useless_function and a_less_useless_function'''
	print x
	a_useless_function(x)
	a_less_useless_function(x)
	return 0
	
some_function(1000)
