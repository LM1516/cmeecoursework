CODE
- pop_gen_prac.R # Calculates departure of genome SNPs from Hardy-Weinburg equilibrium

DATA
- H938_chr15.geno # SNP data used in Code/pop_gen_prac.R

- primate_aligned.fasta # fasta prepared for prac 4

- primate_raw.fasta # fasta prepared for prac 4

RESULTS
- pop_gen_prac_results.pdf # output results from Code/pop_gen_prac.R

SANDBOX
- empty directory that would contain test files
