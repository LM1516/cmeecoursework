''' example accessing, updating and managing SQLite databases'''
__author__ = "Laura Merritt lm1516@ic.ac.uk"
__version__ = "0.0.1"

# import the sqlite3 library
import sqlite3

# create a connection to the database
conn = sqlite3.connect('../Data/test.db')

# to execute commands, create a "cursor"
c = conn.cursor()

# use the cursor to execute the queries
# use the triple single quote to write queries on several lines
c.execute('''CREATE TABLE test
			(ID INTEGER PRIMARY KEY,
			MyVal1 INTEGER,
			MyVal2 TEXT)''')

#~ c.execute('''DROP TABLE test''')

# insert the records. note that because we set the primary key, it will auto-increment. Therefore set it to NULL
c.execute('''INSERT INTO test VALUES
			(NULL, 3, 'mickey')''')

c.execute('''INSERT INTO test VALUES
			(NULL, 4, 'mouse')''')

# when you "commit", all the command will be executed
conn.commit()

# now we select the records
c.execute("SELECT * FROM TEST")

# access the next record:
print c.fetchone()
print c.fetchone()

# let's get all the records at once
c.execute("SELECT * FROM TEST")
print c.fetchall()

# insert many records at once:
# create a list of tuples
manyrecs = [(5, 'goofy'),
			(6, 'donald'),
			(7, 'duck')]

# now call executemany
c.executemany('''INSERT INTO test
				VALUES(NULL, ?, ?)''', manyrecs)

# and commit
conn.commit()

# now lets fetch the records
# we can use the query as an iterator!
for row in c.execute('SELECT * FROM test'):
	print 'Val', row[1], 'Name', row[2]

# close the connection before exiting
conn.close()

##################################
# creating a database in memory
##################################

#~ import sqlite3

conn = sqlite3.connect(":memory:")

c = conn.cursor()

c.execute("CREATE TABLE tt (Val TEXT)")

conn.commit()

z = [('a',), ('ab',), ('abc',), ('b',), ('c',)]

c.executemany("INSERT INTO tt VALUES (?)", z)

conn.commit()

c.execute("SELECT * FROM tt WHERE Val LIKE 'a%'").fetchall()

conn.close()
