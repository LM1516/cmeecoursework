CODE
- clifford.test.R # functions for performing the clifford statistical test

- SpatialStats.R # examples of fitting statistical models to spatial data

- zonalstats.py # calculates mean annual temperature and total precipitation within land cover classes in the UK from spatial data

DATA
- EU/bio1_15.tif
- EU/biol1_16.tif
- EU/biol12_15.tif
- EU/biol12_16.tif
- EU/g250_06.tif
# tif data files used in Code/zonalstats.py

- SpatialModelling/avian_richness.tif
- SpatialModelling/elev.tif
- SpatialModelling/mean_aet.tif
- SpatialModelling/mean_temp.tif
# tif data files used in Code/SpatialStats.R

RESULTS
- SpatialStatsResults.pdf # results output from Code/SpatialStats.R

- zonalstats.csv # results output from Code/zonalstats.py

SANDBOX
- empty directory that would contain test files
