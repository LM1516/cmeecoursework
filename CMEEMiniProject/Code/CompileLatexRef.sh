#!/bin/bash

rm $1.pdf
rm ../Results/$1.pdf
latexmk -pdf $1.tex

# move pdf to results directory
mv $1.pdf ../Results/

if [ -s ../Results/$1.pdf ]
then
	evince ../Results/$1.pdf &
else
	echo "$1.pdf is empty."
fi

## cleanup
rm -f *.log
rm -f *.fls
rm -f *.fdb_latexmk
rm -f *.aux
rm -f *.blg
