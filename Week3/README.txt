CODE
- apply1.R # examplifies use of apply to matrices

- apply2.R # examplifies creation of a function to use within apply

- basic_io.R # examplifies sourcing and exporting data

- boilerplate.R # examplifies creating and testing a function

- break.R # examplifies use of break in while and if loops

- browse.R # examplifies plotting from function

- control.R # examplifies use of control statements

- DataWrang.R # examplifies reading from metadata and converting to long format for analysis

- get_TreeHeight.R # uses csv file argument from command line to calculate height of trees

- next.R # examplifies use of next in for and if loops

- PP_Latice.R # returns in csv file mean and median for subsets of Data/EcolArchives-E089-51-D1.csv

- PP_Regress_loc.R # returns in csv file coefficients of linear models for subsets of Data/EcolArchives-E089-51-D1.csv (location, feeding interaction type, lifestage)

- PP_Regress.R # returns in csv file coefficients of linear models for subsets of Data/EcolArchives-E089-51-D1.csv (feeding interaction type, lifestage)

- run_get_TreeHeight.sh # tests running of get_TreeHeight.sh

- sample.R # simulation of sampling from a population

- SQLinR.R # examplifies databases in R

- TAutoCorr.R # calculates autocorrelation p value for temperatures in Data/KeyWestAnnualMeanTemperature.RData

- TAutoCorr.tex # Latex source code for TAutoCorr.R result discussion

- TreeHeight.R #  calculates heights of trees with trees.csv as test, writing results to csv

- try.R # examplifies use of try through sampling within population

- Vectorize1.R # examplifies how vectorization can speed up running time of code

- Vectorize2.R # Displays times for stochastic (with gaussian fluctuations) Ricker Eqn in vectorized and unvectorized format

DATA
- EcolArchives-E089-51-D1.csv # called by PP_ codes

- KeyWestAnnualMeanTemperature.RData # called by TAutoCorr.R

- PoundHillData.csv # used in practicals

- PoundHillMetaData.csv # called by DataWrang.R

- trees.csv # called by *TreeHeight.R

RESULTS
- fig8.8.pdf # output from Code/PP_Regress.R

- MyData.csv # output from Code/basic_io.R

- PP_Regress_loc_Results.csv # output from Code/PP_Regress_loc.R

- PP_Regress_Results.csv # output from Code/PP_Regress.R

- PP_Results.csv # output from Code/PP_Lattice.R

- Pred_Lattice.pdf # output from Code/PP_Lattice.R

- Pred_Prey_Overlay.pdf # created in practicals

- Prey_Lattice.pdf # output from Code/PP_Latice.R

- SizeRatio_Lattice.pdf # output from Code/PP_Latice.R

- TAutoCorr.pdf # output from Code/TAutoCorr.tex

- TreeHts.csv # output from Code/TreeHeights.R

- trees_treeheights.csv # output from Code/run_get_TreeHeights.sh

SANDBOX
- empty directory for test files
