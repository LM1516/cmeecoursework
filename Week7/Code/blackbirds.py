'''Prints kingdom, phylum and species names for each species in blackbirds.txt input file'''
__author__ = 'Laura Merritt lm1516@ic.ac.uk'
__version__ = '0.0.1'

import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = text.decode('ascii', 'ignore')

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

# finds all examples of a word following "Kingdom"
kingdom_reg = r'Kingdom\s*[a-zA-Z]+'
kingdoms = [str(x) for x in re.findall(kingdom_reg, text)]

# finds all examples of a word following "Phylum"
phylum_reg = r'Phylum\s*[a-zA-Z]+'
phylums = [str(x) for x in re.findall(phylum_reg, text)]

# finds all examples of a word following "Species"
species_reg = r'Species\s*[a-zA-Z]+\s*[a-zA-Z]+'
species = [str(x) for x in re.findall(species_reg, text)]

# for each species prints "blackbird species x phylogeny is" then on new lines prints the kingdom, phylus and species
for i in xrange(len(kingdoms)):
	numb = 1 + i
	print "Blackbird species", numb, "phylogeny is:"
	print kingdoms[i]
	print phylums[i]
	print species[i] + "\n"

