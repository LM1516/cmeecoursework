rm(list=ls())

daphnia <- read.delim("../Data/daphnia.txt")
summary(daphnia)

# checking for outliers
par(mfrow = c(1,2))
plot(Growth.rate ~ Detergent, data = daphnia)
plot(Growth.rate ~ Daphnia, data = daphnia)

# homogeneitry of variances
require(dplyr)

daphnia %>%
	group_by(Detergent) %>%
	summarise(variance=var(Growth.rate))
	
daphnia %>%
	group_by(Daphnia) %>%
	summarise(variance=var(Growth.rate))

# test for normality
dev.off()
hist(daphnia$Growth.rate)

# model daphnia
seFun <- function(x) {
	sqrt(var(x)/length(x))
}
detergentMean <- with(daphnia, tapply(Growth.rate, INDEX = Detergent, FUN = mean))
detergentSEM <- with(daphnia, tapply(Growth.rate, INDEX = Detergent, FUN = seFun))
cloneMean <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = mean))
cloneSEM <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = seFun))

par(mfrow=c(2,1), mar=c(4,4,1,1))
barMids <- barplot(detergentMean, xlab = "Detergent type", ylab = "Population growth rate", ylim = c(0,5))
arrows(barMids, detergentMean - detergentSEM, barMids, detergentMean + detergentSEM, code = 3, angle = 90)
barMids <- barplot(cloneMean, xlab = "Daphnia clone", ylab = "Population growth rate", ylim = c(0,5))
arrows(barMids, cloneMean - cloneSEM, barMids, cloneMean + cloneSEM, code = 3, angle = 90)

daphniaMod <- lm(Growth.rate ~ Detergent + Daphnia, data = daphnia)
anova(daphniaMod)
# residual mean square variation
0.737/1.148
19.589/1.148

summary(daphniaMod)

daphniaANOVAMod <- aov(Growth.rate ~ Detergent + Daphnia, data = daphnia)
summary(daphniaANOVAMod)

daphniaModHSD <- TukeyHSD(daphniaANOVAMod)
daphniaModHSD

dev.off()
par(mfrow=c(2,1), mar=c(4,4,1,1))
plot(daphniaModHSD)

par(mfrow=c(2,2))
plot(daphniaMod)

# Multiple regression
timber <- read.delim("../Data/timber.txt")
summary(timber)

# Outliers
par(mfrow = c(2,2))
boxplot(timber$volume)
boxplot(timber$girth)
boxplot(timber$height)


dev.off()

# Homogeneitry of variances
var(timber$volume)
var(timber$girth)
var(timber$height)

t2 <- as.data.frame(subset(timber, timber$volume!="NA"))
t2$z.girth <- scale(timber$girth)
t2$z.height <- scale(timber$height)
var(t2$z.girth)
var(t2$z.height)
plot(t2)

dev.off()

# is data normally distributed
par(mfrow = c(2,2))
hist(t2$volume)
hist(t2$girth)
hist(t2$height)

dev.off()

# collinearity?
pairs(timber)
cor(timber)
dev.off()
