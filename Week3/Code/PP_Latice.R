MyDF <- read.csv("../Data/EcolArchives-E089-51-D1.csv", header = TRUE)

library(lattice)

# Creates Predator mass by feeding interaction type lattice plot
pdf("../Results/Pred_Lattice.pdf", 11.7, 8.3)
print(densityplot(~log(Predator.mass) | Type.of.feeding.interaction, data = MyDF))
dev.off()

# Creates Prey mass by feeding interaction type lattice plot
pdf("../Results/Prey_Lattice.pdf", 11.7, 8.3)
print(densityplot(~log(Prey.mass) | Type.of.feeding.interaction, data = MyDF))
dev.off()

# Calculates ratio between predator and prey mass
MyDF$Pred.Prey.size.ratio <- MyDF$Prey.mass / MyDF$Predator.mass

# Creates mass ratio by feeding interaction type lattice plot 
pdf("../Results/SizeRatio_Lattice.pdf", 11.7, 8.3)
print(densityplot(~log(Pred.Prey.size.ratio) | Type.of.feeding.interaction, data = MyDF))
dev.off()

# Calculates means for predator mass, prey mass and mass ratio by feeding interaction type
PredMean <- tapply(log(MyDF$Predator.mass), MyDF$Type.of.feeding.interaction, mean)
PreyMean <- tapply(log(MyDF$Prey.mass), MyDF$Type.of.feeding.interaction, mean)
RatioMean <- tapply(log(MyDF$Pred.Prey.size.ratio), MyDF$Type.of.feeding.interaction, mean)

# Calculates medians for predator mass, prey mass and mass ratio by feeding interaction type
PredMedian <- tapply(log(MyDF$Predator.mass), MyDF$Type.of.feeding.interaction, median)
PreyMedian <- tapply(log(MyDF$Prey.mass), MyDF$Type.of.feeding.interaction, median)
RatioMedian <- tapply(log(MyDF$Pred.Prey.size.ratio), MyDF$Type.of.feeding.interaction, median)

# Concatenates means and medians into list that can be used in data frame
Mean <- c(PredMean, PreyMean, RatioMean)
Median <- c(PredMedian, PreyMedian, RatioMedian)

# Creates feeding type list of correct order and length to be used in data frame
feedtype <- c(levels(MyDF$Type.of.feeding.interaction))
numfeedtype <- nlevels(MyDF$Type.of.feeding.interaction)
FeedingType <- rep(feedtype, 3)

# Creates Data type list of correct order and length to be used in data frame
Pred <- rep("LogPredatorMass", numfeedtype)
Prey <- rep("LogPreyMass", numfeedtype)
Ratio <- rep("LogMassRatio", numfeedtype)
DataType <- c(Pred, Prey, Ratio)

# Concatenates results lists into data frame
ResultsData <- data.frame(DataType, FeedingType, Mean, Median)

# Exports data frame to csv file
write.csv(ResultsData, "../Results/PP_Results.csv", row.names = FALSE)
