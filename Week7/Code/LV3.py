""" The density dependent Lotka-Volterra Model simulated using scipy """
__author__ = "Laura Merritt lm1516@ic.ac.uk"
__version__ = "0.0.1"

import sys
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting

# import matplotlip.pylab as p #Some people might need to do this

def dCR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1 - (R/K)) - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dCdt])


# Define parameters:
#~ r = 1. # Resource growth rate
#~ a = 0.1 # Consumer search rate (determines consumption rate) 
#~ z = 1.5 # Consumer mortality rate
#~ e = 0.75 # Consumer production efficiency
r = float(sys.argv[1])
a = float(sys.argv[2])
z = float(sys.argv[3])
e = float(sys.argv[4])

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 15,  1000)

K = float(sys.argv[5])
x0 = int(sys.argv[6])
y0 = int(sys.argv[7])
z0 = sc.array([x0, y0]) # initial conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dCR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

prey, predators = pops.T # What's this for? Transposing
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics \n r = %r a = %r z = %r e = %r K = %r C0 = %r R0 = %r'%(r, a, z, e, K, y0, x0))
#~ p.show()
f1.savefig('../Results/prey_and_predators_3.pdf') #Save figure

# print final population density values
print "Resource population density is %d" % (pops[-1,0])
print "Consumer population density is %d" % (pops[-1,1])
