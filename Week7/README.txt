CODE
- blackbirds.py # Prints kingdom, phylum and species names for each species in Data/blackbirds.txt input file

- DrawFW.py # Plots a snapshot of a food web graph/network

- fmr.R # Plots log(field metabolic rate) against log(body mass) for Data/Nagyetal1999.csv and sorts the list of species names

- LV1.py # The typical Lotka-Volterra Model simulated using scipy

- LV2.py # The density dependent Lotka-Volterra Model simulated using scipy

- LV3.py # The density dependent Lotka-Volterra Model simulated using scipy, prints final population densities

- profileme.py # example functions to test profiling on

- run_fmr_R.py # Runs R script fmr.R showing r terminal information and whether the run was successful

- run_LV.sh # runs and profiles LV?.py codes

- SQLinR.R # Example use of databases in R

- SQLite.py # Example use of databases in python

- TestR.py # Example of using subprocess to run an R script from python

- TestR.R # Example code to use for running R script in python that prints "hello this is R"

- timeitme.py # Example uses of timeit

- using_os.py # Example code using subprocess to catalogue all files/directories below home
that start with C or c

DATA
- Biotraits.db # Example database

- blackbirds.txt # data file called by Code/blackbirds.py

- Consumer.csv # Data used in Biotraits.db

- Nagyetal1999.csv # Data used in fmr.R and run_fmr_R.py

- Resource.csv # Data used in Biotraits.db

- TCP.csv # Data used in Biotraits.db

- test.db # Test database created in Code/SQLite.py

- Test.sqlite # Test database created in Code/SQLinR.R

- TraitInfo.csv # Data used in Biotraits.csv

RESULTS
- prey_and_predators_1.pdf # Output results from Code/LV1.py

- prey_and_predators_2.pdf # Output results from Code/LV2.py

- prey_and_predators_3.pdf # Output results from Code/LV3.py

- TestR_errFile.Rout # Output results from Code/TestR.py

- TestR.Rout # Output results from Code/TestR.py

SANDBOX
- directory containing test files
