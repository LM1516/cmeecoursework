'''Species moved from tuples into dictionary sorted by order'''

__author__ = 'Laura Merritt lm1516@ic.ac.uk'
__version__ = 0.0.1

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:

taxa_dict = {}
for species in taxa: # loops through lines in taxa
	if species[1] in taxa_dict: # checks if the order is within the dictionary
		taxa_dict[species[1]].add(species[0]) # add the species name to the set of that order
	else:
		taxa_dict[species[1]] = set([species[0]]) # both adds the order as a key and adds the species name to the set of that order

# Prints the dictionary so that each order is on a separate line
for key in taxa_dict:
	print key, taxa_dict[key]
