#!/bin/bash
# Author Laura Merritt lm1516@ic.ac.uk
# Script: run_LV.sh
# Desc: runs and profiles files LV1.py and LV2.py 
# Arguements: None needed
# Date Nov 2016


ipython LV1.py
ipython LV2.py 1. 0.1 1.5 0.75 27 10 5
ipython LV3.py 1. 0.1 1.5 0.75 27 10 5

python -m cProfile LV1.py
python -m cProfile LV2.py 1. 0.1 1.5 0.75 27 10 5
python -m cProfile LV3.py 1. 0.1 1.5 0.75 27 10 5
