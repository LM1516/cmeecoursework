''' a series of exercises that print 'hello's followed by 5 foo functions that perform calculations'''

import sys

# How many times will 'hello' be printed?
# 1) Prints 14 'hello's
for i in range(3, 17):
	print 'hello'
	
# 2) Prints 4 hello's
for j in range(12):
	if j % 3 == 0:
		print 'hello'
		
# 3) Prints 5 'hello's
for j in range(15):
	if j % 5 == 3:
		print 'hello'
	elif j % 4 == 3:
		print 'hello'
		
# 4) Prints 5 'hello's
z = 0
while z != 15:
	print 'hello'
	z=z + 3
	
# 5) Prints 8 'hello's
z = 12
while z < 100:
	if z == 31:
		for k in range(7):
			print 'hello'
	elif z == 18:
		print 'hello'
	z = z + 1
	
# What does fooXX do?
def foo1(x):
	'''calculates input argument to the power of 0.5'''
	return x ** 0.5
	
def foo2(x, y):
	'''returns the larger of the two arguments'''
	if x > y:
		return x
	return y
	
def foo3(x, y, z):
	'''returns a string of the input arguments in numerical order'''
	if x > y:
		tmp = y
		y = x
		x = tmp
	if y > z:
		tmp = z
		z = y
		y = tmp
	return [x, y, z]
	
def foo4(x):
	'''returns factorial of input argument beginning calculations at 1'''
	result = 1
	for i in range(1, x + 1):
		result = result * i
	return result
	
# This is a recursive function, meaning the function calls itself
# read about it at
# en.wikipedia.org/wiki/Recursion_(computer_science)
def foo5(x):
	'''returns factorial of input argument beginning calculations at x'''
	if x == 1:
		return 1
	return x * foo5(x-1)
	
def main(argv):
	print foo1(10)
	print foo2(10, 13)
	print foo3(11, 10, 14)
	print foo4(10)
	print foo5(10)
	return 0
	
if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
