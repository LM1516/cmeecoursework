rm(list = ls())

MyDF <- read.csv("../Data/EcolArchives-E089-51-D1.csv", header = TRUE)

# allows for use of ggplot2 package
library(ggplot2)

# initialises ggplot of prey and predator mass with different colours for different predator lifestages
c <- ggplot(MyDF, aes(x = Prey.mass, y = Predator.mass, group = Predator.lifestage, colour = Predator.lifestage)) 
# adds plus shaped points and linear model lines
c <- c + geom_point(shape = 3) + geom_smooth(method = "lm", fullrange = TRUE, size = 0.8) 
# separates into separate grids by type of feeding interaction
c <- c + facet_grid(Type.of.feeding.interaction ~.) 
# moves legend to the bottom and makes sure it's horizontal
c <- c + theme(legend.position = "bottom", legend.direction = "horizontal") 
# changes the x and y axes to log scale, setting axis breaks
c <- c + scale_x_log10(breaks = c(10^-07, 10^-03, 10^+01)) + scale_y_log10(breaks = c(10^-06, 10^-02, 10^+02, 10^+06)) 
# changes axis labels
c <- c + labs(x = "Prey mass in grams", y = "Predator mass in grams")
# changes looks of many sections of graph such as colour and size
c <- c + theme(panel.border = element_rect("grey", fill = NA, size = 0.5),panel.background = element_rect(fill = "white"), panel.grid.major = element_line("grey90"), panel.grid.minor = element_line("grey98"), axis.text.x = element_text(colour = "black"), axis.text.y = element_text(colour = "black"), legend.title = element_text(face = "bold"), strip.background = element_rect(colour = "grey", size = 0.5), aspect.ratio = 0.5) 
# makes legend one row
c <- c + guides(colour = guide_legend(nrow = 1))


# Prints the ggplot to a pdf
pdf("../Results/fig8.8.pdf", 8, 11) 
print(c)  
dev.off()

# makes list of factor levels to use in for loops
feedtype <- c(levels(MyDF$Type.of.feeding.interaction))
lifestage <- c(levels(MyDF$Predator.lifestage))

# initializes first row for result output
Results <- c("FeedType", "PredLifestage", "Slope", "Intercept", "RSquared", "FStatistic", "PValue")

# checks for correct length of subset to create linear model then appends required summary coefficients or NAs to bottom of results matrix
for (i in feedtype) { 
	for (j in lifestage) {
		if (length(MyDF$Predator.mass[(MyDF$Type.of.feeding.interaction == i) & (MyDF$Predator.lifestage == j)]) >= 3) {
			linmod <- lm(MyDF$Predator.mass[(MyDF$Type.of.feeding.interaction == i) & (MyDF$Predator.lifestage == j)] ~ MyDF$Prey.mass[(MyDF$Type.of.feeding.interaction == i) & (MyDF$Predator.lifestage == j)])
			assign((paste(i, j, sep = "")), c(paste(i), paste(j), summary(linmod)[[4]][2], summary(linmod)[[4]][1], summary(linmod)$r.squared, summary(linmod)$fstatistic[[1]], summary(linmod)[[4]][8]))
			Results <- rbind(Results, get(paste(i, j, sep = "")))
		} else {
		assign((paste(i, j, sep = "")), c(paste(i), paste(j), "NA", "NA", "NA", "NA", "NA"))
		Results <- rbind(Results, get(paste(i, j, sep = "")))
		}
	}
}


# regression slope
	# summary(model)[[4]][2]
# regression intercept
	# summary(model)[[4]][1]
# R squared
	# summary(model)$r.squared
# F statistic
	# summary(model)$fstatistic[[1]]
# p value
	# summary(model)[[4]][8]


# Writes results to csv file
write.table(Results, "../Results/PP_Regress_Results.csv", row.names = FALSE, col.names = FALSE, sep = ",")
