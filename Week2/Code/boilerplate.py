#!usr/bin/python

"""Prints: 'This is a boilerplate'"""

__author__ = 'Laura Merritt (lm1516@ic.ac.uk)'
__version__ = '0.0.1'

#imports
import sys #module to interface our program with the operating system

#constants can go here


#functions can go here
def main(argv):
	print 'This is a boilerplate' # Note: indented using two tabs or 4 spaces
	return 0
	
if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
