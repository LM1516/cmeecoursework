#!/bin/bash

rm $1.pdf
rm ../Results/$1.pdf
pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex

# move pdf to results directory
mv $1.pdf ../Results/

if [ -s ../Results/$1.pdf ]
then
	evince ../Results/$1.pdf &
else
	echo "$1.pdf is empty."
fi

## Cleanup
rm -f *~
rm -f *.aux
rm -f *.blg
rm -f *.log
rm -f *.nav
rm -f *.out
rm -f *.snm
rm -f *.toc
rm -f *.vrb
rm -f *.bbl
rm -f *.dvi
rm -f *.lot
rm -f *.lof

