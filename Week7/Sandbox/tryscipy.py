import scipy

# making scipy arrays with integers and floats
a = scipy.array(range(5))
a
type(a)
type(a[0])

a = scipy.array(range(5), float)
a
a.dtype

x = scipy.arange(5)
x
x = scipy.arange(5.)
x
x.shape

# can make scipy arrays like with list comprehensions but without needing to convert
b = scipy.array([i for i in range(100) if i%2==1])
# converting arrays to lists
c = b.tolist()

# working with matrices
mat = scipy.array([[0, 1], [2, 3]])
mat.shape
mat[1]
mat[:,1]
mat[0,0]
mat[1,0]

mat[0,0] = -1
mat

mat[:,0] = [12,12]
mat

scipy.append(mat, [[12,12]], axis = 0)
scipy.append(mat, [[12],[12]], axis = 1)

newRow = [[12,12]]
mat = scipy.append(mat, newRow, axis = 0)
scipy.delete(mat, 2, 0)

mat = scipy.array([[0,1], [2, 3]])
mat0 = scipy.array([[0, 10], [-1, 3]])
scipy.concatenate((mat, mat0), axis = 0)

mat.ravel()
mat.reshape((4,1))
mat.reshape((3,1))

# preallocating arrays
scipy.ones((4,2))
scipy.zeros((4,2))
m = scipy.identity(4)
m
m.fill(16)
m

#~ ?scipy.matrix

mm = scipy.arange(16)
mm = mm.reshape(4,4)
mm.transpose()
mm + mm.transpose()
mm - mm.transpose()
mm * mm.transpose() # is done element wise
mm / mm.transpose()
mm * scipy.pi
mm.dot(mm) # matrix multiplication

import scipy.stats

scipy.stats.norm.rvs(size = 10) # 10 samples from N(0,1)
scipy.stats.norm.rvs(5, size = 10) # change mean to 5
scipy.stats.norm.rvs(5, 100, size = 10) # cchange sd to 100
scipy.stats.randint.rvs(0, 10, size = 7) # random integers between 0 and 10
