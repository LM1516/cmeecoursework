'''Runs R script fmr.R showing r terminal information and whether the run was successful'''
__author__ = 'Laura Merritt lm1516@ic.ac.uk'
__version__ = '0.0.1'

import subprocess as sp
import re
sp.os.system("Rscript fmr.R > ../Results/fmrres.txt")

res = open('../Results/fmrres.txt', 'r').read()
print res
m = re.search(r'Finished\sin\sR!', res)
if m is not None and m.group() == 'Finished in R!':
	print "Run successful"
else:
	print "Run failed"
