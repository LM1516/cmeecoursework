echo "Running MiniProject"
echo "Aligning data"
Rscript AlignData.R
echo "Finished aligning data"
echo "Running linear regressions"
Rscript fit_lm.R
echo "Finished running linear regressions"
echo "Calculating AIC and BIC"
ipython test_AIC.py
echo "Finished calculating AIC and BIC"
echo "Creating graphs"
Rscript create_graphs.R
echo "Finished creating graphs"
echo "Obtaining word count for latex report"
texcount Report.tex -inc -incbib -sum -1 >> wordcount.tmp
echo "Finished obtaining word count for latex report"
echo "Compiling report from latex to pdf"
bash CompileLatexRef.sh Report
echo "Finished compiling report from latex to pdf"
rm wordcount.tmp
