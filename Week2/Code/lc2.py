'''example exercises separating months by rainfall during 1910 using list comprehension and conventional loops'''
__author__ = 'Laura Merritt lm1516@ic.ac.uk'
__version__ = "0.0.1"

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.

rain_over_100_lc = [species for species in rainfall if species[1] > 100]
print rain_over_100_lc
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

months_under_50_lc = [species[0] for species in rainfall if species[1] < 50]
print months_under_50_lc

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

rain_over_100_loops = []
for species in rainfall:
	if species[1] > 100:
		rain_over_100_loops.append(species)
print rain_over_100_loops

months_under_50_loops = []
for species in rainfall:
	if species[1] < 50:
		months_under_50_loops.append(species[0])
print months_under_50_loops

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
