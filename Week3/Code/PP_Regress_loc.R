rm(list = ls())

MyDF <- read.csv("../Data/EcolArchives-E089-51-D1.csv", header = TRUE)

# makes lists of factor levels for use in for loops
location <- c(levels(MyDF$Location))
feedtype <- c(levels(MyDF$Type.of.feeding.interaction))
lifestage <- c(levels(MyDF$Predator.lifestage))

# initializes first row for result output
Results <- c("Location", "FeedType", "PredLifestage", "Slope", "Intercept", "RSquared", "FStatistic", "PValue")

# checks for correct length of subset to create linear model then appends required summary coefficients or NAs to bottom of results matrix
for (k in location) {
	for (i in feedtype) { 
		for (j in lifestage) {
			if (length(MyDF$Predator.mass[(MyDF$Location == k) & (MyDF$Type.of.feeding.interaction == i) & (MyDF$Predator.lifestage == j)]) >= 3) {
				linmod <- lm(MyDF$Predator.mass[(MyDF$Location == k) & (MyDF$Type.of.feeding.interaction == i) & (MyDF$Predator.lifestage == j)] ~ MyDF$Prey.mass[(MyDF$Location == k) & (MyDF$Type.of.feeding.interaction == i) & (MyDF$Predator.lifestage == j)])
				assign((paste(k, i, j, sep = "")), c(paste(k), paste(i), paste(j), summary(linmod)[[4]][2], summary(linmod)[[4]][1], summary(linmod)$r.squared, summary(linmod)$fstatistic[[1]], summary(linmod)[[4]][8]))
				Results <- rbind(Results, get(paste(k, i, j, sep = "")))
			} else {
			assign((paste(k, i, j, sep = "")), c(paste(k), paste(i), paste(j), "NA", "NA", "NA", "NA", "NA"))
			Results <- rbind(Results, get(paste(k, i, j, sep = "")))
			}
		}
	}
}

# regression slope
	# summary(model)[[4]][2]
# regression intercept
	# summary(model)[[4]][1]
# R squared
	# summary(model)$r.squared
# F statistic
	# summary(model)$fstatistic[[1]]
# p value
	# summary(model)[[4]][8]


# Writes results to csv file
write.table(Results, "../Results/PP_Regress_loc_Results.csv", row.names = FALSE, col.names = FALSE, sep = ",")
