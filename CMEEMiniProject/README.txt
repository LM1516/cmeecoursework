CODE
-AlignData.R # takes factor data from csv files and compiles into csv files containing a single year's data

-CompileLatexRef.sh # converts tex document into pdf when called without .tex extension

-create_graphs.R # creates graphs from data and models to put into tex file

-fit_lm.R # fits linear models to individual year and combined data then exports for use in AIC and BIC calculations

-Report.bbl # File needed for texcount to work in run_MiniProject.sh

-Report.tex # LaTeX file containing code for final miniproject report

-run_MiniProject.sh # runs all miniproject codes and creates a word count for input into the latex file before then compiling it

-test_AIC.py # calculates AIC and BIC for significant linear regression models against combined (total) data set

DATA
-Data1990.csv # Factor data for 1990

-Data2000.csv # Factor data for 2000

-Data2005.csv # Factor data for 2005

-DataTotalminO.csv # Factor data for 1990, 2000 and 2005

-forest_land.csv # Forest cover data for 1990, 2000 and 2005

-GDPpercapita.csv # GDP data for 1990, 2000 and 2005

-Modelsmino.csv # Models generated through linear regressions

-primary_forest_land.csv # Primary forest cover data for 1990, 2000 and 2005

-wood_products_removal.csv # Wood product removal data for 1990, 2000 and 2005

REFERENCES
-ReportBiblio.bib # bibtex references for latex report

RESULTS
-MinOutlierGraph.pdf # plots of model assumptions once Lesotho was removed

-ModelFitGraph.pdf # graph for report with combined data points and model lines

-OutlierGraph.pdf # plots of model assumptions before Lesotho was removed

-Report.pdf # Complation of latex document as final version of report

-test_AICResultsminO.csv # Models, AIC and BIC calculated from data
