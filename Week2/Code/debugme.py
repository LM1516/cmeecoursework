"""example code that creates a bug (dividing by 0) and opens ipdb to allow checking of bug location"""

__author__ = "Laura Merritt lm1516@ic.ac.uk"
__version__ = "0.0.1"

def createabug(x) :
	y = x**4
	z = 0.
	import ipdb; ipdb.set_trace()
	y = y/z
	return y
	
createabug(25)
