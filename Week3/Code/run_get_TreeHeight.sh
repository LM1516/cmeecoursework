#!/bin/bash
echo -e "\n Running get_TreeHeight.R \n"

Rscript get_TreeHeight.R "../Data/trees.csv"

cat ../Results/trees_treeheights.csv
