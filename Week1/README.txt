CODE
-boilerplate.sh # prints 'This is a shell script' on new line

-CompileLaTeX.sh # compiles and opens pdf from LaTeX file

-ConcatenateTwoFiles.sh # merges 2 files into a new third file

-CountLines.sh # prints the number of lines in the file within a sentence

-csvtospace.sh # converts a csv file to a text file

-FirstBiblio.bib # Contains bibtex reference data for Einstein 1905

-FirstExample.pdf # pdf from FirstExample.tex

-FirstExample.tex # Example LaTeX file

-MyExampleScript.sh # prints 'hello user' twice

-tabtocsv.sh # converts files with tab separaters to csv files

-UnixPrac1.txt # commands to perform a series of actions on files in Week1/Data/fasta

-variables.sh # shell script showing use of variables


DATA
-fasta # directory containing E.coli genome fasta files

-spawannxs.txt # example data file for practicing grep

-Temperatures # directory containing csv files for testing csvtospace.sh and
				resulting text files


SANDBOX
Contains test directories and files
