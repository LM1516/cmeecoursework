CODE
-align_seqs.py # prints the best alignment of two sequences in a .csv file to a .txt results file

-align_seqs_fasta # prints the best alignment of sequences from two separate fasta files to a text file

-basic_csv.py # imports a csv file into python then writes the 1st and 5th element of each row to a new file

-basic_io.py # example commands for file input, output and storing objects

-boilerplate.py # prints: 'This is a boilerplate'

-cfexercises.py # a series of exercises that print 'hello's followed by 5 foo functions that perform calculations

-control_flow.py # some functions exemplifying the use of control statements

-debugme.py # example code that creates a bug (dividing by 0) and opens ipdb to allow checking bug location

-dictionary.py # species moved from tuples into dictionary sorted by order

-lc1.py # example exercises showing the use of list comprehensions and conventional loops

-lc2.py # example exercises separating months by rainfall during 1910 using list comprehension and conventional loops

-loops.py # example code showing how loops can be used

-oaks.py # example exercises in list comprehension returning names of oaks from a list containing tree species

-regexs.py # example code showing use of regular expressions for finding information within strings

-scope.py # example code showing the assignment of global variables

-sysargv.py # example code showing ways of gaining information from a module eg. sys

-test_control_flow.py # example doctest on a simple function

-test_oaks.py # takes data from TestOaksData.csv and returns which species are oaks in JustOaksData.csv

-tuple.py # prints the species information (latin name, common name, mass) on separate lines

-using_name.py # example code that tests whether the program is being run by itself or imported from another module

DATA
- TestOaksData.csv # example data file for practicing list comprehensions

- JustOaksData.csv # example ouput file of sorted data

RESULTS
- directory in which results can be stored, currently only contains a test file

SANDBOX
- contains test files
