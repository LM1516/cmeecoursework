\documentclass[11pt]{article}
\title{Potential for predicting wood product removal from single year data of influencing factors}
\renewcommand{\thefootnote}{\arabic{footnote}}
\setcounter{footnote}{0}
\author{Laura Merritt\textsuperscript{1}}
\date{Word Count: \input{wordcount.tmp}}
\usepackage{lineno}
\usepackage{setspace}
\doublespacing
\usepackage{graphicx, caption}
\usepackage[section]{placeins}
\begin{document}

\maketitle

\hspace*{\fill}\textsuperscript{1} Imperial College London, Silwood Park Campus\hspace*{\fill}

\newpage
\linenumbers

\section{Abstract}
Forests are an important ecosystem that influence both biosphere processes and biodiversity levels but they are under threat of degradation for a variety of reasons. One of these is wood product removal which involves both deforestation and selective logging practices. As these are the main drivers of forest degradation it is important to be able to predict wood product removal, and therefore know where to focus conservation efforts. GDP, forest cover, and percentage of primary forest cover were investigated as potential factors affecting wood product removal. Both GDP and percentage of primary forest cover were found to have no significant effect on wood product removal, however forest cover did have a significant positive effect. It was also found that linear regression models for multi-year data does not have significantly better fit to the combined data than models generated from a single year. This suggests that single year data sets can potentially be used as predictive tools for wood product removal, which is useful for countries where data is limited.

\section{Introduction}
Forests cover approximately one third of the earths surface, and are therefore a major contributor to biosphere processes. The surface albedo, carbon dioxide levels, moisture within the atmosphere and ozone and methane concentrations are all aspects that are influenced by the presence of forests, causing strong warming impacts on the climate when disturbed through deforestation \cite{scott2016including}. 

Through loss of habitat, species deaths during clearance, and extinction debt, biodiversity is also negatively impacted by forest degradation \cite{norris2016ecology}. The extinction debt in particular is predicted to potentially cause a 120\% increase in 20th century extinctions of bird, mammal and amphibian species \cite{rosa2016environmental}. Deforestation is not the only cause of forest degradation as other anthropogenic disturbances such as selective logging, wildfires, and edge, area and isolation effects can also cause large decreases in conservation value \cite{barlow2016anthropogenic}. Selective logging is often considered to have little effect on biodiversity due to meta-analysis studies showing few significant differences in biodiversity within taxa. This has however been suggested, particularly in birds, to be due to a change in species composition with generalist species replacing forest specialists \cite{burivalova2014thresholds}.

A pervasive factor influencing forest degradation is wood product removal as, while there are many additional causes for deforestation, selective logging is primarily focussed on the gain of wood product. It is therefore important to be able to predict wood product removal of countries as a basis for understanding likely areas of forest degradation. GDP \cite{allen1985causes}, forest cover, and percentage of primary forest cover \cite{fenning2002will} are considered as potential factors influencing wood product removal, with an investigation into the possibility of using models generated from one year's data as predictors for trends shown in multi-year data.

\section{Methods}
Forest cover, primary forest cover and wood product removal data has been collated from the Food and Agriculture Organisation, United Nations. GDP data has been collated from the World Bank. Lesotho has been removed from all data sets due to a combination of factors differing from other countries that are likely to cause a disproportional wood product removal compared to the size of the country \cite{marakeenvironmental, allen1985causes}. Data was analysed for 1990, 2000 and 2005 as these were consistent years across all data sets. Linear regressions were fitted to log transformed data per year and to the combined years, then Akaike information criterion (AIC) and Bayesion information criterion (BIC) were calculated against the combined data set.

\section{Results}
From simple linear regressions, no significant effects of GDP or percentage of primary forest cover on wood product removal were found (p \textgreater 0.05). Forest cover has a sigificant positive effect on wood product removal (\textbf{1990}: F\textsubscript{1,62} = 76.03, p \textless 0.05, R\textsuperscript{2} = 0.5436. \textbf{2000}: F\textsubscript{1,73} = 66.46, p \textless 0.05, R\textsuperscript{2} = 0.4694 \textbf{2005}: F\textsubscript{1,68}, p \textless 0.05, R\textsuperscript{2} = 0.4954 \textbf{combined}: F\textsubscript{1,207} = 212.4, p \textless 0.05, R\textsuperscript{2} = 0.5041). The regression lines are: $Y=-1.00+1.03X$, $Y=-0.72+1.01X$ and $Y=-1.23+1.04X$ respectively for 1990, 2000 and 2005. For the combined data, the regression line is $Y=-0.98+1.03X$. There are no significant differences of model fit to the combined data between the individual year or average models (AIC and BIC differences \textless 0.94, Figure \ref{fig:1}).

\begin{figure}[!ht]
\captionsetup{width=0.88\textwidth}
\centering
\includegraphics[width=0.9\textwidth]{../Results/ModelFitGraph.pdf}
\centering
\caption{Log forest cover (ha) against log wood product removal (m\textsuperscript{3}). The lines are linear regression lines calculated for the 1990 (AIC: 856.93, BIC: 866.96), 2000 (AIC: 856.17, BIC: 866.20) and 2005 (AIC: 856.31, BIC: 866.34) data individually, and the combined data set (AIC: 856.00, BIC: 866.03) of these lines.}
\label{fig:1}
\end{figure}

\section{Discussion}
GDP was expected to potentially have a negative effect on wood product removal as there is more financial pressure to use available resources in countries with lower GDPs \cite{allen1985causes}. Percentage of forest that is primary was also expected to negatively influence wood product removal as secondary forest could be plantations which are often used as a less invasive source of wood products \cite{fenning2002will}. However, neither of these potential contributing factors where found to have a significant effect on wood product removal which suggests there are other stronger factors.

Wood product removal was expected to be positively affected by forest cover, as there is more potential for wood to be removed. This was found to be true as there is a significant positive relationship between forest cover and wood product removal. This allowed an investigation into the use of regression models from single year data as predictors for trends shown in combined data of multiple years. Importantly there was no significant difference in model fit between single year models and the one calculated from the combined data. This suggests that few changes happened to the trend in the 15 year period. As there is no significant difference between multiple years this suggests that forest cover could potentially be a factor that could be used in predicting countries that are likely to produce more wood product, allowing targeting of conservation strategies to the countries that are likely to have greatest impact due to primary forest degredation. The use of single year data sets also allows for better predictive capabilities with countries that are data limited.

Biodiversity loss and carbon dioxide levels are already major issues that are being contributed to by forest degradation \cite{scott2016including, rosa2016environmental}. Here we have shown that two factors that cannot be used to predict wood product removal are GDP and primary percentage of forest cover. However, forest cover has been shown to be a potential factor for predicting wood product removal. This is beneficial information to inform conservation priorities, however only approximately 50\% of the variation within the data is explained by these models. The additional variation suggests that there are additional influencing factors that have not been considered within this study. Further study should therefore be targeted at finding the additional factors that will improve the models' predictive abilities to increase our capability to respond effectively to this threat.

\bibliographystyle{apalike}
\bibliography{../References/ReportBiblio}
\end{document}
