\begin{thebibliography}{}

\bibitem[Allen and Barnes, 1985]{allen1985causes}
Allen, J.~C. and Barnes, D.~F. (1985).
\newblock The causes of deforestation in developing countries.
\newblock {\em Annals of the Association of American Geographers}, pages
  163--184.

\bibitem[Barlow et~al., 2016]{barlow2016anthropogenic}
Barlow, J., Lennox, G., Ferreira, J., Berenguer, E., Lees, A., Nally, R.,
  Thomson, J., Ferraz, S., Louzada, J., Oliveira, V., et~al. (2016).
\newblock Anthropogenic disturbance in tropical forests can double biodiversity
  loss from deforestation.
\newblock {\em Nature}, 535(7610):144.

\bibitem[Burivalova et~al., 2014]{burivalova2014thresholds}
Burivalova, Z., {\c{S}}ekercio{\u{g}}lu, {\c{C}}.~H., and Koh, L.~P. (2014).
\newblock Thresholds of logging intensity to maintain tropical forest
  biodiversity.
\newblock {\em Current biology}, 24(16):1893--1898.

\bibitem[Fenning and Gershenzon, 2002]{fenning2002will}
Fenning, T.~M. and Gershenzon, J. (2002).
\newblock Where will the wood come from? plantation forests and the role of.
\newblock {\em TRENDS in Biotechnology}, 20(7).

\bibitem[Marake and Molumeli, 1999]{marakeenvironmental}
Marake, M.~V. and Molumeli, P.~A. (1999).
\newblock Environmental management in lesotho.
\newblock {\em Planning, Policies and Politics in Eastern and Southern Africa},
  page~80.

\bibitem[Norris, 2016]{norris2016ecology}
Norris, K. (2016).
\newblock Ecology: The tropical deforestation debt.
\newblock {\em Current Biology}, 26(16):R770--R772.

\bibitem[Rosa et~al., 2016]{rosa2016environmental}
Rosa, I.~M., Smith, M.~J., Wearn, O.~R., Purves, D., and Ewers, R.~M. (2016).
\newblock The environmental legacy of modern tropical deforestation.
\newblock {\em Current Biology}, 26(16):2161--2166.

\bibitem[Scott et~al., 2016]{scott2016including}
Scott, C., Monks, S., Spracklen, D., Arnold, S., Forster, P., Rap, A., Carslaw,
  K., Chipperfield, M., Reddington, C., and Wilson, C. (2016).
\newblock Including the biogeochemical impacts of deforestation increases
  projected warming of climate.
\newblock In {\em EGU General Assembly Conference Abstracts}, volume~18, page
  15602.

\end{thebibliography}
