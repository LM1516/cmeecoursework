rm(list=ls())
d <- read.table("../Data/SparrowSize.txt", header = TRUE)

d1 <- subset(d, d$Wing != "NA")
model3 <- lm(Wing~as.factor(BirdID), data = d1)
anova(model3)

require(dplyr)

d1 %>%
	group_by(BirdID) %>%
	summarise(count = length(BirdID)) %>%
		summarise(length(BirdID))
	
# to get denominator	
d1 %>%
	group_by(BirdID) %>%
	summarise(count = length(BirdID)) %>%
	summarise(sum(count))
	
# to get numerator
d1 %>%
	group_by(BirdID) %>%
	summarise(counts = length(BirdID)) %>%
	summarise(sum(counts^2))
	
#calculate N0 for wing from bird id
(1/617)*(1659-7307/1695)
# repeatability for wing from bird id
((13.20-1.62)/2.74)/(1.62+(13.2-1.62)/2.74)
	
#for body mass
d2 <- subset(d, d$Mass != "NA")


# might keep variable in lm if parameter estimate is large as still biologically relevant
