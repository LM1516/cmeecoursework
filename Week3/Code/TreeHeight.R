# This function calculates heights of trees
# from the angle of elevation and the distance
# from the base using the trigonometric formula
# height = distance * tan(radians)
#
# Arguments:
# degrees		The angle of elevation
# distance      The distance from base
#
# Output:
# The height of the tree, same units as "distance"

TreeHeight <- function(degrees, distance)
{
	radians <- degrees * pi / 180
	height <- distance * tan(radians)
#~ 	print(paste("Tree height is:", height))
	return(height)
}

Trees <- read.csv("../Data/trees.csv", header = TRUE)
Trees$Tree.Height.m<-TreeHeight(Trees$Angle.degrees, Trees$Distance.m)
write.csv(Trees, "../Results/TreeHts.csv")

#~ TreeHeight(37, 40)
