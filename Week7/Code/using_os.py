""" Example code using subprocess to catalogue all files/directories below home
that start with C or c"""

__author__ = "Laura Merritt lm1516@ic.ac.uk"
__version__ = "0.0.1"

# Use the subprocess.os module to get a list of files and  directories 
# in your ubuntu home directory 

# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess
import re

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []

# Use a for loop to walk through the home directory.
for (dir, subdir, files) in subprocess.os.walk(home):
	for directory in subdir:
		if directory.startswith("C"):
			FilesDirsStartingWithC.append(directory)
	for file in files:
		if file.startswith("C"):
			FilesDirsStartingWithC.append(file)
		
print '\n' + "The Files and Directories that begin with C are: "
print FilesDirsStartingWithC
		  
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Type your code here:

# Create a list to store the results
FilesDirsStartingWithCc = []

# Use a for loop to walk through the home directory
for (dir, subdir, files) in subprocess.os.walk(home):
	for directory in subdir:
		if directory.startswith("C") or directory.startswith("c"):
			FilesDirsStartingWithCc.append(directory)
	for file in files:
		if file.startswith("C") or file.startswith("c"):
			FilesDirsStartingWithCc.append(file)
		
print '\n' + "The Files and Directories that begin with C or c are: "
print FilesDirsStartingWithCc



#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

# Type your code here:

# Create a list to store the results
DirsStartingWithCc = []

# Use a for loop to walk through the home directory
for (dir, subdir, files) in subprocess.os.walk(home):
	for directory in subdir:
		if directory.startswith("C") or directory.startswith("c"):
			DirsStartingWithCc.append(directory)
		
print '\n' + "The Directories that begin with C or c are: "
print DirsStartingWithCc
