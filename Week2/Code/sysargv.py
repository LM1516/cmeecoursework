"""Example code showing ways of gaining information from a module eg. sys"""

__author__ = "Laura Merritt lm1516@ic.ac.uk"
__version__ = "0.0.1"

import sys
print "This is the name of the script", sys.argv[0]
print "Number of arguements: ", len(sys.argv)
print "The arguements are: ", str(sys.argv)
