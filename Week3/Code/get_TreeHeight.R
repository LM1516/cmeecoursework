#! /usr/bin/env Rscript

# This function calculates heights of trees
# from the angle of elevation and the distance
# from the base using the trigonometric formula
# height = distance * tan(radians)
#
# Arguments:
# One csv file containing 3 columns:
# tree identification
# degrees		The angle of elevation (must be in third column of input data)
# distance      The distance from base (must be in second column of input data)
#
# Output:
# The height of the tree, same units as "distance"

args <- commandArgs(TRUE) # Allows input of arguments from command line

Trees <- read.csv(args[1], header = TRUE) #imports data from file given in argument


TreeHeight <- function(degrees, distance) # function to use on file
{
	radians <- degrees * pi / 180
	height <- distance * tan(radians)
	return(height)
}

Trees$Tree.Height.m<-TreeHeight(Trees[,3], Trees[,2]) # calls data from specific columns to use in function
	
# replaces sections of file name to create output file related to file name given in argument
outFile <- file.path("../Results/", gsub(".csv", "_treeheights.csv", args[1]))
outFile <- file.path("../Results/", gsub("../Data/", "../Results/", outFile))

write.csv(Trees, outFile) # writes the modified dataframe to the new results file
