#!/usr/bin/python
#Filename: using_python.py

"""example code that tests whether the program is being run by itself or imported from another module"""

__author__ = "Lauar Merritt lm1516@ic.ac.uk"
__version__ = "0.0.1"

if __name__ == '__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module'
